const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000
const User = require("./controller/user")
const jsonParser = bodyParser.json()


const user = new User

app.get("/", (req, res) => {
    res.json({
        "tes" : "server"
    })
})

app.get("/user", user.getUser)
app.get("/user/:index", user.getDetailedUser)
app.post("/user", jsonParser, user.insertUser)
app.put("/user/:index", jsonParser, user.updateUser)
app.delete("/user/:index",user.deleteUser)


app.listen(port, () => {
    console.log("server jalan")
})
