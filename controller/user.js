const { successResponse } = require('../helper/response')


class User {
    constructor() {
       this.user = 
       [
           {
           "fullName": "abi komara",
           "username": "abikomara",
           "password": "abi123",
           "email": "abi@gmail.com"
           },
           {
            "fullName": "andi andi",
            "username": "andi",
            "password": "andi123",
            "email": "andi@gmail.com"
            }
       ]
    }

    getUser = (req, res) => {
        successResponse(res, 200, this.user, 
            // { total: this.user.length }
        )
    }

    getDetailedUser = (req, res) => {
        const index = req.params.index
        successResponse(res, 200, this.user[index])
    }
    
/*
        res.status(200).json({
            data: this.user[index],
            meta: {
                code: 200,
                message: "sukses ambil User"
            }
        })
*/

    insertUser = (req, res) => {
        const body = req.body
        const param = {
            "fullName": body.fullName,
            "username": body.username,
            "password": body.password,
            "email": body.email
        }

        this.user.push(param)
        successResponse(res, 201, param)
    }
        
/*
        res.status(201). json({
            data : param,
            meta: {
                code: 201,
                message: "sukses insert User"
            }
        })      
*/

    updateUser = (req, res) => {
        const index = req.params.index
        const body = req.body
        
        this.user[index].fullName = body.fullName
        this.user[index].username = body.username
        this.user[index].password = body.password
        this.user[index].email = body.email

        successResponse(res, 200, this.user[index])
    }
        /*res.status(200). json({
            data: this.user[index],
            meta: {
                code: 201,
                message: "sukses update User"
            }
        })*/      

    

    deleteUser = (req, res) => {

        const index = req.params.index
        this.user.splice(index, 1)

        successResponse(res, 200, null)
    }
        /*res.status(200).json({
            data: null,
            meta: {
                code:200,
                message: "User dihapus"
            }
        })*/
    
}

module.exports = User